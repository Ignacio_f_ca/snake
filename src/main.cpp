#include <iostream>

#include <SFML/Graphics.hpp>

#include <engine.hpp>

#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 720

int main()
{
	Engine engine;
	engine.run();
	return 0;
}