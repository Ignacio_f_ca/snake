#include <engine.hpp>

const sf::Time Engine::timePerFrame = sf::seconds(1.f / 60.f);

Engine::Engine()
{
    this->resolution = sf::Vector2f(800, 600);
    this->window.create(sf::VideoMode(resolution.x, resolution.y), "Snake", sf::Style::Default);
    this->window.setFramerateLimit(Engine::fps);

    this->score = 0;
    this->currentLevel = 0;
    this->sectionsToAdd = 0;
    this->applesEatenTotal = 0;
    this->applesEatenThisLevel = 0;

    this->checkLevelFiles();

    this->mainFont.loadFromFile("./assets/fonts/Inconsolata-Regular.ttf");

    this->marginTopTextsPositions = -9;

    this->currentGameState = Engine::GameState::STARTING;

    setupText(&this->titleText, this->mainFont, "Snake", 28, sf::Color::Blue);
    sf::FloatRect titleTextBounds = this->titleText.getLocalBounds();
    titleText.setPosition(sf::Vector2f(this->resolution.x / 2 - titleTextBounds.width / 2, this->marginTopTextsPositions));

    setupText(&this->currentLevelText, mainFont, "level 1", 24, sf::Color::Blue);
    currentLevelText.setPosition(sf::Vector2f(15, this->marginTopTextsPositions));
    sf::FloatRect currentLevelTextBounds = currentLevelText.getGlobalBounds();

    setupText(&this->applesEatenText, mainFont, "apples 0", 24, sf::Color::Blue);
    sf::FloatRect applesEatenTextBounds = currentLevelText.getGlobalBounds();
    this->applesEatenText.setPosition(sf::Vector2f(currentLevelTextBounds.left + applesEatenTextBounds.width + 20, this->marginTopTextsPositions));

    // this->scoreText.setString(std::to_string(this->score));
    // sf::FloatRect scoreTextBounds = this->scoreText.getLocalBounds();
    // this->scoreText.setPosition(sf::Vector2f(this->resolution.x - scoreTextBounds.width - 15, this->marginTopTextsPositions));

    setupText(&this->scoreText, mainFont, std::to_string(score), 24, sf::Color::Blue);
    sf::FloatRect scoreTextBounds = this->scoreText.getLocalBounds();
    this->scoreText.setPosition(sf::Vector2f(this->resolution.x - scoreTextBounds.width - 15, this->marginTopTextsPositions));

    setupText(&this->startGameText, mainFont, "\t\t   Snake game \n use arrows to play, esc to scape \n \t\t and p to pause", 32, sf::Color::Yellow);
    sf::FloatRect startGameTextBounds = this->startGameText.getLocalBounds();
    this->startGameText.setPosition(sf::Vector2f(this->resolution.x / 2 - startGameTextBounds.width / 2, 100));

    setupText(&this->pressEnterText, mainFont, "Press ENTER to try again", 38, sf::Color::Yellow);
    sf::FloatRect pressEnterTextBounds = this->pressEnterText.getLocalBounds();
    pressEnterText.setPosition(sf::Vector2f(resolution.x / 2 - pressEnterTextBounds.width / 2, 200));
    pressEnterText.setOutlineColor(sf::Color::Black);
    pressEnterText.setOutlineThickness(2);

    setupText(&this->gameOverText, mainFont, "GAME OVER", 72, sf::Color::Yellow);
    sf::FloatRect gameOverTextBounds = this->gameOverText.getLocalBounds();
    gameOverText.setPosition(sf::Vector2f(resolution.x / 2 - gameOverTextBounds.width / 2, 100));
    gameOverText.setOutlineColor(sf::Color::Black);
    gameOverText.setOutlineThickness(2);
}

void Engine::startGame()
{
    speed = 4;
    timeSinceLastMove = sf::Time::Zero;

    wallSections.clear();

    this->directionQueue.clear();
    snakeDirection = Engine::Direction::RIGHT;
    this->sectionsToAdd = 0;

    this->applesEatenThisLevel = 0;
    this->applesEatenTotal = 0;

    this->currentGameState = Engine::GameState::RUNNING;
    this->lastGameState = this->currentGameState;

    this->currentLevel = 1;
    this->loadLevel(this->currentLevel);

    this->currentLevelText.setString("level " + std::to_string(currentLevel));

    this->applesEatenText.setString("apples " + std::to_string(applesEatenTotal));

    this->scoreText.setString(std::to_string(this->score));

    newSnake();
    moveApple();
}

void Engine::beginNextLevel()
{
    this->currentLevel = this->currentLevel + 1;
    this->wallSections.clear();
    this->directionQueue.clear();
    this->speed = 2 + this->currentLevel;
    this->snakeDirection = Engine::Direction::RIGHT;
    this->sectionsToAdd = 0;
    this->applesEatenThisLevel = 0;

    loadLevel(this->currentLevel);
    newSnake();
    moveApple();

    currentLevelText.setString("level " + std::to_string(currentLevel));
    sf::FloatRect currentLevelTextBounds = currentLevelText.getGlobalBounds();
    this->applesEatenText.setPosition(sf::Vector2f(currentLevelTextBounds.left + currentLevelTextBounds.width + 20, this->marginTopTextsPositions));
}

void Engine::input()
{
    sf::Event event{};

    while (window.pollEvent(event))
    {
        // Window closed
        if (event.type == sf::Event::Closed)
        {
            window.close();
        }

        // Handle Keyboard Input
        if (event.type == sf::Event::KeyPressed)
        {
            // Quit
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                window.close();
            }
            // Pause
            if (event.key.code == sf::Keyboard::P)
            {
                this->togglePause();
            }

            // New Game
            if (event.key.code == sf::Keyboard::Enter &&
                (this->currentGameState == Engine::GameState::GAMEOVER || this->currentGameState == Engine::GameState::STARTING))
            {
                startGame();
            }

            // Cheat keys
            if(event.key.code == sf::Keyboard::Space) // add snake sections
            {
                this->sectionsToAdd += 2;
            }
            if(event.key.code == sf::Keyboard::M) // move apple
            {
                this->moveApple();
            }
            if(event.key.code == sf::Keyboard::L) // begin next level
            {
                if( this->currentLevel < this->maxLevels )
                {
                    this->beginNextLevel();
                }
            }
        }
    }
    // Direction
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        addDirection(Engine::Direction::UP);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        addDirection(Engine::Direction::DOWN);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        addDirection(Engine::Direction::LEFT);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        addDirection(Engine::Direction::RIGHT);
    }
}

void Engine::addDirection(int newDirection)
{
    if (directionQueue.empty())
    {
        directionQueue.emplace_back(newDirection);
    }
    else
    {
        if (directionQueue.back() != newDirection)
        {
            directionQueue.emplace_back(newDirection);
        }
    }
}

void Engine::draw()
{
    window.clear(sf::Color::Black);

    for (Wall &wall : this->wallSections)
    {
        window.draw(wall.getShape());
    }

    window.draw(this->apple.getSprite());

    for (SnakeSection &snakeSection : snake)
    {
        sf::RectangleShape section = snakeSection.getShape();
        // std::cout << "SnakePos<"<<snakeSection.getPosition().x << ", " << snakeSection.getPosition().y << ">, section<"<<section.getPosition().x << ", " << section.getPosition().y << ">"<< std::endl;
        window.draw(section);
    }

    window.draw(this->titleText);
    window.draw(this->currentLevelText);
    window.draw(this->applesEatenText);
    window.draw(this->scoreText);

    if (this->currentGameState == Engine::GameState::GAMEOVER || this->currentGameState == GameState::STARTING)
    {
        if (this->currentGameState == GameState::GAMEOVER)
            window.draw(this->gameOverText);
        else
            window.draw(this->startGameText);
        window.draw(this->pressEnterText);
    }

    window.display();
}

void Engine::update()
{
    // update snake positions
    if (timeSinceLastMove.asSeconds() >= sf::seconds(1.f / float(speed)).asSeconds())
    {
        sf::Vector2f thisSectionPosition = snake[0].getPosition();
        sf::Vector2f lastSectionPosition = thisSectionPosition;

        if (!directionQueue.empty())
        {
            switch (snakeDirection)
            {
            case Engine::Direction::UP:
                if (directionQueue.front() != Engine::Direction::DOWN)
                {
                    snakeDirection = directionQueue.front();
                }
                break;
            case Engine::Direction::DOWN:
                if (directionQueue.front() != Engine::Direction::UP)
                {
                    snakeDirection = directionQueue.front();
                }
                break;
            case Engine::Direction::RIGHT:
                if (directionQueue.front() != Engine::Direction::LEFT)
                {
                    snakeDirection = directionQueue.front();
                }
                break;
            case Engine::Direction::LEFT:
                if (directionQueue.front() != Engine::Direction::RIGHT)
                {
                    snakeDirection = directionQueue.front();
                }
                break;
            }
            directionQueue.pop_front();
        }

        this->score += snake.size() + (applesEatenTotal + 1);
        this->scoreText.setString(std::to_string(this->score));
        sf::FloatRect scoreTextBounds = this->scoreText.getLocalBounds();
        this->scoreText.setPosition(sf::Vector2f(this->resolution.x - scoreTextBounds.width - 15, this->marginTopTextsPositions));

        // check if snake grow
        if (this->sectionsToAdd)
        {
            this->addSnakeSection();
            --this->sectionsToAdd;
        }

        // Update snake's head
        switch (snakeDirection)
        {
        case Engine::Direction::RIGHT:
            snake[0].setPosition(sf::Vector2f(thisSectionPosition.x + 20, thisSectionPosition.y));
            break;
        case Engine::Direction::LEFT:
            snake[0].setPosition(sf::Vector2f(thisSectionPosition.x - 20, thisSectionPosition.y));
            break;
        case Engine::Direction::UP:
            snake[0].setPosition(sf::Vector2f(thisSectionPosition.x, thisSectionPosition.y - 20));
            break;
        case Engine::Direction::DOWN:
            snake[0].setPosition(sf::Vector2f(thisSectionPosition.x, thisSectionPosition.y + 20));
            break;
        }

        // Update snake tail
        for (int i = 1; i < snake.size(); ++i)
        {
            thisSectionPosition = snake[i].getPosition();
            snake[i].setPosition(lastSectionPosition);
            lastSectionPosition = thisSectionPosition;
        }
        
        // collision with apple, increase snake and check if load next level
        if (this->snake[0].getShape().getGlobalBounds().intersects(this->apple.getSprite().getGlobalBounds()))
        {
            this->applesEatenThisLevel += 1;
            this->applesEatenTotal += 1;
            applesEatenText.setString("apples " + std::to_string(applesEatenTotal));
            sf::FloatRect currentLevelTextBounds = currentLevelText.getGlobalBounds();
            this->applesEatenText.setPosition(sf::Vector2f(currentLevelTextBounds.left + currentLevelTextBounds.width + 20, this->marginTopTextsPositions));

            bool begingNewLevel = false;
            if (this->applesEatenThisLevel >= applesForNextLevel && this->currentLevel < this->maxLevels)
            {
                begingNewLevel = true;
                beginNextLevel();
            }

            if (!begingNewLevel)
            {
                this->sectionsToAdd += 2;
                this->speed++;
                this->moveApple();
            }
        }

        // Collision dectection - Snake Body
        for (int i = 1; i < snake.size(); ++i)
        {
            if (this->snake[0].getShape().getGlobalBounds().intersects(this->snake[i].getShape().getGlobalBounds()))
            {
                this->currentGameState = GameState::GAMEOVER;
            }
        }

        // Collision dectection - Walls
        for (Wall &wall : this->wallSections)
        {
            if (this->snake[0].getShape().getGlobalBounds().intersects(wall.getShape().getGlobalBounds()))
            {
                this->currentGameState = GameState::GAMEOVER;
            }
        }

        // Reset the last move timer
        timeSinceLastMove = sf::Time::Zero;
    }
}

void Engine::newSnake()
{
    snake.clear();
    // snake.emplace_back(sf::Vector2f(20, 580));

    snake.emplace_back(sf::Vector2f(100, 100));
    snake.emplace_back(sf::Vector2f(80, 100));
    snake.emplace_back(sf::Vector2f(60, 100));
}

void Engine::addSnakeSection()
{
    sf::Vector2f newSectionPosition = snake[snake.size() - 1].getPosition();
    snake.emplace_back(newSectionPosition);
}

void Engine::moveApple()
{
    // find location to place apple
    //  not inside snake or wall

    // divide field into sections the size of the apple - remove 2 to eclude exterior walls
    sf::Vector2f appleResolution(this->resolution.x / 20 - 2, this->resolution.y / 20 - 2);
    sf::Vector2f newAppleLocation;

    bool badLocation = false;
    std::srand(time(nullptr));
    do
    {
        badLocation = false;
        newAppleLocation.x = (float)(1 + rand() / ((RAND_MAX + 1u) / (int)appleResolution.x)) * 20;
        newAppleLocation.y = (float)(1 + rand() / ((RAND_MAX + 1u) / (int)appleResolution.y)) * 20;

        for (SnakeSection &snakeSection : snake)
        {
            if (snakeSection.getShape().getGlobalBounds().intersects(sf::Rect<float>(newAppleLocation.x, newAppleLocation.y, 20, 20)))
            {
                badLocation = true;
                break;
            }
        }

        for (Wall &wall : this->wallSections)
        {
            if (wall.getShape().getGlobalBounds().intersects(sf::Rect<float>(newAppleLocation.x, newAppleLocation.y, 20, 20)))
            {
                badLocation = true;
                break;
            }
        }

    } while (badLocation);
    this->apple.setPosition(newAppleLocation);
}

void Engine::togglePause()
{
    if (this->currentGameState == Engine::GameState::RUNNING)
    {
        this->lastGameState = this->currentGameState;
        this->currentGameState = Engine::GameState::PAUSED;
    }
    else if (this->currentGameState == Engine::GameState::PAUSED)
    {
        this->currentGameState = this->lastGameState;
    }
}

void Engine::checkLevelFiles()
{
    this->maxLevels = 0;
    this->levels.clear();
    // load level manifests file
    std::ifstream levelsManifest("assets/levels.txt");
    std::ifstream testFile;
    for (std::string manifestLine; std::getline(levelsManifest, manifestLine);)
    {
        testFile.open("assets/" + manifestLine);
        if (testFile.is_open())
        {
            this->levels.emplace_back("assets/" + manifestLine);
            testFile.close();
            this->maxLevels++;
        }
    }
}

void Engine::loadLevel(int levelNumber)
{
    std::string levelFile = this->levels[levelNumber - 1];

    std::ifstream level(levelFile);
    std::string line;

    if (level.is_open())
    {
        for (int row = 0; row < 30; ++row)
        {
            std::getline(level, line);
            for (int column = 0; column < 40; ++column)
            {
                if (line[column] == 'x')
                {
                    this->wallSections.emplace_back(sf::Vector2f(column * 20, row * 20), sf::Vector2f(20, 20));
                }
            }
        }
        level.close();
    }
}

void Engine::setupText(sf::Text *textItem, const sf::Font &font, const std::string &value, int size, sf::Color colour)
{
    textItem->setFont(font);
    textItem->setString(value);
    textItem->setCharacterSize(size);
    textItem->setFillColor(colour);
    
}

// Main loop in this function
void Engine::run()
{
    sf::Clock clock;

    // Main loop - Runs until the window is closed
    while (window.isOpen())
    {
        sf::Time dt = clock.restart();

        if (this->currentGameState == GameState::PAUSED || this->currentGameState == GameState::GAMEOVER || this->currentGameState == GameState::STARTING)
        {
            input();
            if (this->currentGameState == GameState::GAMEOVER || this->currentGameState == GameState::STARTING)
            {
                draw();
            }
            sf::sleep(sf::milliseconds(2)); // do't peg the cpu

            continue;
        }

        timeSinceLastMove += dt;

        input();
        update();
        draw();
    }
}