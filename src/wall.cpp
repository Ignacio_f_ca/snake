#include <wall.hpp>

Wall::Wall(sf::Vector2f position, sf::Vector2f size)
{
    this->wallShape.setSize(size);
    this->wallShape.setPosition(position);
    this->wallShape.setFillColor(sf::Color::White);
}


sf::RectangleShape Wall::getShape()
{
    return this->wallShape;
}