#include <apple.hpp>
#include <iostream>

Apple::Apple()
{
    sf::Vector2f startPositiong(-400, -300);
    this->sprite.setSize(sf::Vector2f(20, 20));
    this->sprite.setFillColor(sf::Color::Red);
    this->sprite.setPosition(startPositiong);
}

void Apple::setPosition(sf::Vector2f newPosition)
{
    this->sprite.setPosition(newPosition);
}

sf::RectangleShape Apple::getSprite()
{
    return this->sprite;
}

