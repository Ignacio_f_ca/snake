#include <snakesection.hpp>

SnakeSection::SnakeSection(sf::Vector2f startPosition)
{
    this->section.setSize(sf::Vector2f(20, 20));
    this->section.setFillColor(sf::Color::Green);
    this->section.setPosition(startPosition);
}

sf::Vector2f SnakeSection::getPosition()
{
    sf::FloatRect shapeBounds = this->section.getGlobalBounds();
    return sf::Vector2f(shapeBounds.left, shapeBounds.top);
}

void SnakeSection::setPosition(sf::Vector2f newPosition)
{
    this->section.setPosition(newPosition);
}

sf::RectangleShape SnakeSection::getShape()
{
    return this->section;
}
