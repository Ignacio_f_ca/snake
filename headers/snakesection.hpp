#pragma once


#include <SFML/Graphics.hpp>

class SnakeSection
{
private:
    sf::RectangleShape section;
public:
    // explicit
    SnakeSection(sf::Vector2f startPosition);

    sf::Vector2f getPosition();
    
    void setPosition(sf::Vector2f newPosition);

    sf::RectangleShape getShape();
};
