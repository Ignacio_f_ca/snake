#pragma once 

#include <vector>
#include <deque>
#include <fstream>
#include <string>
#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <snakesection.hpp>
#include <apple.hpp>
#include <wall.hpp>

class Engine
{
private:
    sf::Vector2f resolution; 
    sf::RenderWindow window;
    const unsigned int fps = 60;
    static const sf::Time timePerFrame;

    std::vector<SnakeSection> snake;

    int snakeDirection;
    std::deque<int> directionQueue; // queue for direction key presses
    int speed;

    sf::Time timeSinceLastMove;
    
    Apple apple;
    int sectionsToAdd;
    int applesEatenThisLevel;
    int applesEatenTotal;
    int applesForNextLevel = 4;

    unsigned long long int score;

    int currentGameState;
    int lastGameState; // for storing the last state the games was in then pausing

    std::vector<Wall> wallSections;
    std::vector<std::string> levels;
    int currentLevel;
    int maxLevels;

    int marginTopTextsPositions;
    sf::Font mainFont;
    sf::Text titleText;
    sf::Text applesEatenText;
    sf::Text currentLevelText;
    sf::Text scoreText;
    sf::Text gameOverText;
    sf::Text pressEnterText;
    sf::Text startGameText;
public:
    enum Direction{ UP, RIGHT, DOWN, LEFT};
    enum GameState{ RUNNING, PAUSED, GAMEOVER, STARTING};

    Engine();

    void startGame();

    void draw();

    void input();

    void update();

    void newSnake();
    void addSnakeSection();
    void addDirection(int newDirection);

    void moveApple();
    void checkLevelFiles();
    void loadLevel(int levelNumber);

    void beginNextLevel();
    
    void togglePause();

    static void setupText(sf::Text *textItem, const sf::Font &font, const std::string &value, int size, sf::Color colour);

    //Main loop in this function
    void run();

    
};

