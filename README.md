# Snake

Snake game for Windows based on [CaffeinatedTech](https://www.youtube.com/watch?v=2fizsE5ynTM&list=PLbPaYYCufiXzbKTwPpYpgkUplgsCscEm6): Nibbles - Snake game with C++ and SFML.

It's made with:
- CMake 
- SFML (static compiled)
- MSYS2 mingw

To compile it, you can download this proyect and only need CMake and Mingw.

```
cd Snake
mkdir build 
cd build
cmake -G "MinGW Makefiles" .. 
cmake --build .
```

And you will have the 'Snake.exe' on the 'build' folder.

To execute it only need the executable itself and the folder 'assets' in the same folder where you execute the program.